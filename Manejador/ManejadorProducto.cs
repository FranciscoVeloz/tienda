﻿using Entidad;
using AccesoDatos;
using System.Data;

namespace Manejador
{
    public class ManejadorProducto
    {
        ConexionBD conexion = new ConexionBD();

        //Insertar datos
        public string Insertar(Producto producto)
        {
            return conexion.Comando($"insert into producto values(null, '{producto.Nombre}', '{producto.Descripcion}', '{producto.Precio}')");
        }

        //Eliminar datos
        public string Eliminar(Producto producto)
        {
            return conexion.Comando($"delete from producto where idproducto = '{producto.IdProducto}'");
        }

        //Actualizar datos
        public string Actualizar(Producto producto)
        {
            return conexion.Comando($"update producto set nombre = '{producto.Nombre}', descripcion = '{producto.Descripcion}'," +
                $" precio = '{producto.Precio}' where idproducto = '{producto.IdProducto}'");
        }

        //Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}