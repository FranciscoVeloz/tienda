﻿namespace Entidad
{
    public class Producto
    {
        public int IdProducto { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public double Precio { get; set; }

        public Producto(int idproducto, string nombre, string descripcion, double precio) => 
            (IdProducto, Nombre, Descripcion, Precio) =
            (idproducto, nombre, descripcion, precio);
    }
}
