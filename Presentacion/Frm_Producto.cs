﻿using System;
using System.Windows.Forms;
using Entidad;
using Manejador;

namespace Presentacion
{
    public partial class Frm_Producto : Form
    {
        Producto producto = new Producto(0, "", "", 0);

        ManejadorProducto manejador = new ManejadorProducto();

        int id = 0;

        public Frm_Producto()
        {
            InitializeComponent();
        }

        void LimpiarTxt()
        {
            TxtNombre.Clear();
            TxtDescripcion.Clear();
            TxtPrecio.Clear();
        }

        void ActualizarDtg()
        {
            DtgProducto.DataSource = manejador.Mostrar($"select * from producto", "producto").Tables[0];
            DtgProducto.AutoResizeColumns();
            LimpiarTxt();
            TxtNombre.Focus();
        }

        private void Frm_Producto_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            string resultado = manejador.Insertar(new Producto(0, TxtNombre.Text, TxtDescripcion.Text, double.Parse(TxtPrecio.Text)));
            ActualizarDtg();
        }

        private void DtgProducto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            producto.IdProducto = int.Parse(DtgProducto.Rows[e.RowIndex].Cells[0].Value.ToString());
            producto.Nombre = DtgProducto.Rows[e.RowIndex].Cells[1].Value.ToString();
            producto.Descripcion = DtgProducto.Rows[e.RowIndex].Cells[2].Value.ToString();
            producto.Precio = double.Parse(DtgProducto.Rows[e.RowIndex].Cells[3].Value.ToString());

            id = producto.IdProducto;
            TxtNombre.Text = producto.Nombre;
            TxtDescripcion.Text = producto.Descripcion;
            TxtPrecio.Text = producto.Precio.ToString();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar al producto: { producto.Nombre}?", 
                "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejador.Eliminar(producto);
                ActualizarDtg();
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejador.Actualizar(producto = new Producto(id, TxtNombre.Text, TxtDescripcion.Text, 
                double.Parse(TxtPrecio.Text)));
            ActualizarDtg();
        }
    }
}
