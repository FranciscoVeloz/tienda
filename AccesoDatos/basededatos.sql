﻿
--Creamos la base de datos
create database tienda;

use database tienda;

--Creamos la tabla producto
create table producto (
	idproducto int primary key auto_increment,
	nombre varchar(100),
	descripcion varchar(100),
	precio double
);

describe producto;