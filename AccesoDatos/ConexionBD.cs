﻿using System.Data;
using Bases;

namespace AccesoDatos
{
    public class ConexionBD
    {
        Conectar conectar = new Conectar("localhost", "root", "", "tienda");

        public string Comando(string query)
        {
            return conectar.Comando(query);
        }

        public DataSet Mostrar(string query, string table)
        {
            return conectar.Consultar(query, table);
        }
    }
}
